module.exports = class LogicError extends Error {
  constructor(status, type, message) {
    super(message);
    this.status = status;
    this.type = type;
  }
  static InternalServer() {
    return new LogicError(
      500,
      "INTERNAL_SERVER_ERROR",
      "Something went wrong please try again later"
    );
  }
};
