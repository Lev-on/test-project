const { User } = require("../models");
const LogicError = require("./error.js");

const get_user_by_id = async (id) => {
  try {
    const user = await User.findByPk(id).then((data) => data.dataValues);
    console.log("user-=-=-", user);
    return user;
  } catch (error) {
    return LogicError.InternalServer();
  }
};

const create_user = async ({ name, owner }) => {
  try {
    const user = await User.create({ name, owner });
    return user;
  } catch (error) {
    return LogicError.InternalServer();
  }
};

module.exports = { get_user_by_id, create_user };
