require("dotenv").config();
const express = require("express");
const app = express();
const BodyParser = require("body-parser");

const PORT = process.env.PORT || 4000;
var jsonParser = BodyParser.json();

app.use("/api", jsonParser, require("./web/router.js"));

app.listen(PORT, () => {
  console.log(`Server started at ${PORT} port.`);
});
