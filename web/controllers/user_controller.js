const { get_user_by_id, create_user } = require("../../logic/main_logic.js");

const index = async (req, res) => {
  try {
    const user = await get_user_by_id(req.params.id);
    return res.status(200).json(user);
  } catch (error) {
    return res.status(500).json("Something went wrong");
  }
};

const create = async (req, res) => {
  try {
    const user = await create_user(req.body);

    return res.status(201).json("User created.");
  } catch (error) {
    return res.status(500).json("Something went wrong");
  }
};

module.exports = { index, create };
