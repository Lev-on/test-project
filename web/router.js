const Router = require("express").Router();

const { index, create } = require("./controllers/user_controller.js");
Router.get("/account/:id", index);
Router.post("/account", create);

module.exports = Router;
